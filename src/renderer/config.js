let config = {}

let status = process.env.NODE_ENV === 'development'

config.server = (status) ? 'localhost' : '212.237.59.145'
config.port = (status) ? '8888' : '80'

export { config }
