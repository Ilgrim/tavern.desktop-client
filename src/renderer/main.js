import Vue from 'vue'
import axios from 'axios'

import App from './App'
import router from './router'
import store from './store'

if (!process.env.IS_WEB) Vue.use(require('vue-electron'))
Vue.http = Vue.prototype.$http = axios
Vue.config.productionTip = false

let s1 = new Promise((resolve, reject) => {
  let script = document.createElement('script')
  script.onload = () => {
    resolve()
  }
  script.src = 'https://cdnjs.cloudflare.com/ajax/libs/uikit/3.0.0-beta.42/js/uikit.min.js'
  document.head.appendChild(script)
})

let s2 = s1.then(() => {
  return new Promise((resolve, reject) => {
    let script = document.createElement('script')
    script.onload = () => {
      resolve()
    }
    script.src = 'https://cdnjs.cloudflare.com/ajax/libs/uikit/3.0.0-beta.42/js/uikit-icons.min.js'
    document.head.appendChild(script)
  })
})

s2.then(() => {
  /* eslint-disable no-new */
  new Vue({
    components: { App },
    router,
    store,
    template: '<App/>'
  }).$mount('#app')

  router.push('manga')
})
