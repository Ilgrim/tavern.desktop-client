/* eslint-disable eol-last,no-trailing-spaces */
import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      component: require('@/components/Main').default,
      children: [
        {
          path: 'news',
          component: require('@/components/contents/News').default
        },
        {
          path: 'chat',
          component: require('@/components/chat').default
        },
        {
          path: 'manga',
          component: require('@/components/contents/Catalog').default
        },
        {
          path: 'comic',
          component: require('@/components/contents/Catalog').default
        },
        {
          path: 'faq',
          component: require('@/components/contents/Faq').default
        },
        {
          path: 'catalog/:id',
          component: require('@/components/contents/catalog/ItemPage').default
        },
        {
          path: 'reader/:chapterIndex',
          component: require('@/components/contents/catalog/Reader').default
        }
      ]
    }
  ]
})

/* {
      path: '/',
      name: 'landing-page',
      component: require('@/components/LandingPage').default
    },
{
  path: '*',
    redirect: '/'
} */