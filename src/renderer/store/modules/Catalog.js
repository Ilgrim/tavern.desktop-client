import axios from 'axios'
import {config} from '../../config'

const state = {
  meta: {
    count: 0,
    status: false
  },
  catalog: {
    manga: [],
    comics: [],
    page: 0,
    scrollPosition: 0
  },
  search: {
    manga: [],
    keyword: '',
    skip: 0,
    page: 0,
    scrollPosition: 0,
    byAuthor: {
      status: false,
      authorId: '',
      skip: 0
    },
    byType: {
      status: false,
      typeId: '',
      skip: 0
    }
  },
  skip: {
    manga: 0,
    comics: 0
  }
}

const mutations = {
  SET_META (state, count) {
    state.meta.count = count
  },
  SET_CATALOG (state, newItems) {
    state.catalog.manga.push(...newItems)
  },
  SET_CATALOG_PAGE (state, page) {
    state.catalog.page = page
  },
  SET_CATALOG_SCROLL_POSITION (state, position) {
    state.catalog.scrollPosition = position
  },
  SEARCH_RESPONSE (state, arr) {
    state.search.manga = arr
  },
  SEARCH_KEYWORD (state, keyword) {
    state.search.keyword = keyword
  },
  SEARCH_DEL (state) {
    state.search.manga = []
  },
  LOADING_STATUS (state, status) {
    state.meta.status = status
  },
  CHANGE_SEARCH_SKIP (state, skip) {
    state.search.skip = skip
  },
  CHANGE_SKIP (state, skip) {
    state.skip.manga = skip
  },
  SET_SEARCH_PAGE (state, page) {
    state.search.page = page
  },
  SET_SEARCH_SCROLL_POSITION (state, position) {
    state.search.scrollPosition = position
  },
  SET_STATUS_SEARCH_BY_AUTHOR (state, status) {
    state.search.byAuthor.status = status
  },
  SET_AUTHOR_ID_SEARCH_BY_AUTHOR (state, authorId) {
    state.search.byAuthor.authorId = authorId
  },
  SET_SKIP_SEARCH_BY_AUTHOR (state, skip) {
    state.search.byAuthor.skip = skip
  },
  SET_STATUS_SEARCH_BY_TYPE (state, status) {
    state.search.byType.status = status
  },
  SET_TYPE_ID_SEARCH_BY_TYPE (state, typeId) {
    state.search.byType.typeId = typeId
  },
  SET_SKIP_SEARCH_BY_TYPE (state, skip) {
    state.search.byType.skip = skip
  }
}

const actions = {
  setCatalog ({ dispatch, commit, state }, green = false) {
    if (state.catalog.manga.length === 0 || green) {
      dispatch('loadingStatus', true)
      axios({
        method: 'get',
        url: `http://${config.server}:${config.port}/manga`,
        params: {skip: state.skip.manga},
        withCredentials: true
      })
        .then(response => {
          commit('SET_CATALOG', response.data.content.list)
          commit('SET_META', response.data.content.count)
          dispatch('loadingStatus', false)
        })
    }
  },
  flipPage ({ dispatch, commit }, skip) {
    console.log(skip)
    commit('CHANGE_SKIP', skip)
    dispatch('setCatalog', true)
  },
  setCatalogPage ({ dispatch, commit }, page) {
    commit('SET_CATALOG_PAGE', page)
  },
  setCatalogScrollPosition ({ dispatch, commit }, position) {
    commit('SET_CATALOG_SCROLL_POSITION', position)
  },
  search ({ dispatch, commit, state }, keyword = '') {
    let query = (keyword !== '') ? keyword : state.search.keyword
    if (keyword !== state.search.keyword) {
      commit('SET_SEARCH_PAGE', 1)
      commit('CHANGE_SEARCH_SKIP', 0)
    }
    commit('SET_STATUS_SEARCH_BY_AUTHOR', false)
    commit('SET_STATUS_SEARCH_BY_TYPE', false)
    dispatch('loadingStatus', true)
    console.log(query)
    axios({
      method: 'get',
      url: `http://${config.server}:${config.port}/manga/search`,
      params: {query: query, skip: state.search.skip},
      withCredentials: true
    })
      .then(response => {
        commit('SEARCH_KEYWORD', keyword)
        commit('SEARCH_RESPONSE', response.data.content)
        dispatch('loadingStatus', false)
      }).catch(error => {
        console.log(error)
      })
  },
  searchByAuthor ({ dispatch, commit, state }, authorId) {
    let queryAuthorId = (authorId !== '') ? authorId : state.search.byAuthor.authorId
    if (authorId !== state.search.byAuthor.authorId) {
      commit('SET_SKIP_SEARCH_BY_AUTHOR', 0)
    }
    commit('SET_STATUS_SEARCH_BY_AUTHOR', true)
    commit('SET_STATUS_SEARCH_BY_TYPE', false)
    dispatch('loadingStatus', true)
    axios({
      method: 'get',
      url: `http://${config.server}:${config.port}/manga/search`,
      params: {query: '', authorId: queryAuthorId, skip: state.search.byAuthor.skip},
      withCredentials: true
    })
      .then(response => {
        commit('SET_AUTHOR_ID_SEARCH_BY_AUTHOR', queryAuthorId)
        commit('SEARCH_RESPONSE', response.data.content)
        dispatch('loadingStatus', false)
      }).catch(error => {
        console.log(error)
      })
  },
  searchByType ({ dispatch, commit, state }, typeId) {
    let queryTypeId = (typeId !== '') ? typeId : state.search.byType.typeId
    if (typeId !== state.search.byType.typeId) {
      commit('SET_SKIP_SEARCH_BY_TYPE', 0)
    }
    console.log('#', queryTypeId)
    commit('SET_STATUS_SEARCH_BY_TYPE', true)
    commit('SET_STATUS_SEARCH_BY_AUTHOR', false)
    dispatch('loadingStatus', true)
    axios({
      method: 'get',
      url: `http://${config.server}:${config.port}/manga/search`,
      params: {query: '', typeId: queryTypeId, skip: state.search.skip},
      withCredentials: true
    })
      .then(response => {
        commit('SET_TYPE_ID_SEARCH_BY_TYPE', queryTypeId)
        commit('SEARCH_RESPONSE', response.data.content)
        dispatch('loadingStatus', false)
      }).catch(error => {
        console.log(error)
      })
  },
  searchDel ({ commit }) {
    commit('SEARCH_DEL')
    commit('SET_SEARCH_PAGE', 0)
  },
  searchFlipPage ({ dispatch, commit, state }, skip) {
    commit('CHANGE_SEARCH_SKIP', skip)
    if (state.search.byAuthor.status) {
      dispatch('searchByAuthor', state.search.byAuthor.authorId)
    }
    if (state.search.byType.status) {
      dispatch('searchByType', state.search.byType.typeId)
    }
    if (!state.search.byAuthor.status && !state.search.byType.status) {
      dispatch('search', state.search.keyword)
    }
  },
  loadingStatus ({ commit }, status) {
    commit('LOADING_STATUS', status)
  },
  setSearchPage ({ dispatch, commit }, page) {
    commit('SET_SEARCH_PAGE', page)
  },
  setSearchScrollPosition ({ dispatch, commit }, position) {
    commit('SET_SEARCH_SCROLL_POSITION', position)
  }
}

export default {
  state,
  mutations,
  actions
}
