import axios from 'axios'
import {config} from '../../config'

const state = {
  profile: {
    login: '',
    password: ''
  },
  auth: false
}

const getters = {
  profile: state => {
    return state.profile
  }
}

const mutations = {
  SET_PROFILE (state, data) {
    state.profile = data
  },
  SET_AUTH (state, status) {
    state.auth = status
  }
}

const actions = {
  registration ({ dispatch, commit }, regForm) {
    return new Promise((resolve, reject) => {
      let data = new FormData()
      data.set('invite', regForm.invite)
      data.set('login', regForm.login)
      data.set('password', regForm.password)
      data.set('displayName', regForm.displayName)
      axios.post(`http://${config.server}:${config.port}/registration`, data)
        .then(response => {
          if (!response.data.error.status) {
            commit('SET_PROFILE', response.data.content)
            resolve()
          }
        })
        .catch(error => {
          errorHandle(error, reject)
        })
    })
  },
  login ({ dispatch, commit, state }, logForm) {
    return new Promise((resolve, reject) => {
      let data = new FormData()
      data.set('login', logForm.login)
      data.set('password', logForm.password)
      axios({
        method: 'post',
        url: `http://${config.server}:${config.port}/login`,
        data: data,
        withCredentials: true
      })
        .then(response => {
          if (!response.data.error.status) {
            commit('SET_AUTH', true)
            commit('SET_PROFILE', response.data.content)
            resolve()
          }
        })
        .catch(error => {
          errorHandle(error, reject)
        })
    })
  },
  setProfile ({ dispatch, commit }, data) {
    commit('SET_PROFILE', data)
  }
}

function errorHandle (error, reject) {
  if (!error.response) {
    let error = {status: true, message: 'Ошибка подключения к интернету!'}
    reject(error)
  } else {
    reject(error.response.data.error)
  }
}

export default {
  state,
  getters,
  mutations,
  actions
}
