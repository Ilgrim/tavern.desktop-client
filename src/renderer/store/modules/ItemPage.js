import axios from 'axios'
import {config} from '../../config'

const state = {
  id: '',
  item: {},
  chapters: {}
}

const mutations = {
  SET_ITEM (state, data) {
    state.item[data.id] = data.item
    state.id = data.id
  },
  SET_CHAPTERS (state, data) {
    state.chapters[state.id] = data.item
  }
}

const actions = {
  setItem ({ commit, state }, id) {
    if (!(id in state.item)) {
      return new Promise((resolve) => {
        if (state.id !== id || state.item === null) {
          axios({
            method: 'get',
            url: `http://${config.server}:${config.port}/manga/${id}`,
            withCredentials: true
          })
            .then(response => {
              commit('SET_ITEM', {item: response.data.content, id: id})
              resolve()
            })
        }
      })
    }
  },
  setChapters ({ commit, state }, chapters) {
    if (!(state.id in state.chapters)) {
      commit('SET_CHAPTERS', {item: chapters})
    }
  }
}

export default {
  state,
  mutations,
  actions
}
