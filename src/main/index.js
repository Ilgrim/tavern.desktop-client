'use strict'

import { app, BrowserWindow } from 'electron'
import { autoUpdater } from './autoupdate'
import {config} from '../renderer/config'
import axios from 'axios/index'
const path = require('path')
const ipc = require('electron').ipcMain
require('./ipc')

autoUpdater.logger = require('electron-log')
autoUpdater.logger.transports.file.level = 'info'

/**
 * Set `__static` path to static files in production
 * https://simulatedgreg.gitbooks.io/electron-vue/content/en/using-static-assets.html
 */
if (process.env.NODE_ENV !== 'development') {
  global.__static = require('path').join(__dirname, '/static').replace(/\\/g, '\\\\')
}

let userId = null

ipc.on('user-id', (event, id) => {
  userId = id
})

let mainWindow
const winURL = process.env.NODE_ENV === 'development'
  ? `http://localhost:9080`
  : `file://${__dirname}/index.html`

function createWindow () {
  /**
   * Initial window options
   */
  mainWindow = new BrowserWindow({
    height: 720,
    useContentSize: true,
    width: 1280,
    frame: true,
    webPreferences: {
      experimentalFeatures: true,
      webSecurity: false
    },
    icon: path.join(__dirname, '/icons/icon.png')
  })

  mainWindow.loadURL(winURL)
  mainWindow.setMenu(null)

  mainWindow.on('close', (e) => {
    axios({
      method: 'get',
      url: `http://${config.server}:${config.port}/logout?id=${userId}`,
      withCredentials: true
    })
  })

  mainWindow.on('closed', () => {
    mainWindow = null
  })
}

app.on('ready', () => {
  createWindow()
  if (process.env.NODE_ENV === 'production') autoUpdater.checkForUpdatesAndNotify()
})

app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('activate', () => {
  if (mainWindow === null) {
    createWindow()
  }
})
