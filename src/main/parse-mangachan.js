const request = require('request')
const cheerio = require('cheerio')
const uuid = require('uuid/v5')
const url = require('url')
const path = require('path')
const fs = require('fs-path')

let status = process.env.NODE_ENV === 'development'
let tmpPath = './tmp/'
let parse = {}

parse.chapters = (source) => {
  return new Promise((resolve, reject) => {
    let chapters = []
    request(source, null, (error, response, body) => {
      if (error) console.log(error)
      let $ = cheerio.load(body)
      if ($.text().includes('Манга удалена')) {
        resolve({error: true, msg: 'Источник удалил мангу по запросу правообладателя!'})
      }
      $('table.table_cha tr.zaliv, table.table_cha tr.no_zaliv').each((key, val) => {
        let a = $(val).find('div.manga a')
        let title = getName(a.text())
        let source = 'http://mangachan.me' + a.attr('href')
        chapters.push({uid: uuid(source, uuid.URL), title: title, source: source})
      })
      resolve(chapters)
    })
  })
}

function getName (srcTitle) {
  const r1 = /\d{1,}[.]\d{1,}/
  const r2 = /[ - ]\d{1,}/
  let res = []
  let m = srcTitle.match(r2)
  if (m !== -1 && m !== null) {
    res = m
  }
  m = srcTitle.match(r1)
  if (m !== -1 && m !== null) {
    res = m
  }

  if (res.length > 0) {
    let name = srcTitle.split(res[0].trim())[1].trim()
    return 'Глава ' + res[0].trim() + ((name === '') ? '' : ': ' + name)
  } else {
    return srcTitle
  }
}

parse.pages = (event, chapter) => {
  let chapterPath = tmpPath + chapter.uid
  return new Promise((resolve, reject) => {
    let fss = require('fs')
    if (fss.existsSync(chapterPath)) {
      getPagesFromFs(chapterPath, resolve)
    } else {
      getPagesFromSource(event, chapter, chapterPath, resolve, reject)
    }
  })
}

function getPagesFromFs (chapterPath, resolve) {
  fs.find(chapterPath, function (err, list) {
    if (err) console.log(64, err)
    if (process.env.NODE_ENV === 'development') {
      resolve(list.files)
    } else {
      let arr = []
      list.files.forEach((uri) => {
        arr.push(path.resolve(uri))
      })
      resolve(arr)
    }
  })
}
function getPagesFromSource (event, chapter, chapterPath, resolve, reject) {
  request(chapter.source, null, (error, response, body) => {
    if (error) reject(error)

    let start = body.indexOf('fullimg')
    let one = body.substring(start)
    let end = one.indexOf(']')
    let two = one.substring(0, (end - 1))

    let arr = two
      .replace('fullimg":[', '')
      .replace(/"/g, '')
      .split(',')
    downloadPages(event, arr, chapterPath, resolve)
  })
}

function downloadPages (event, arr, chapterPath, resolve) {
  let count = 0
  arr.forEach(function (item) {
    let parsed = url.parse(item)
    let filePath = chapterPath + '/' + path.basename(parsed.pathname)

    request.get({url: item, encoding: null, timeout: 50000}, (e, r, body) => {
      if (!e) {
        fs.writeFile(filePath, body, 'binary', function (err) {
          if (err) {
            console.log(103, err)
          } else {
            count += 1
            if (count === arr.length) {
              getPagesFromFs(chapterPath, resolve)
            }
            let msg = `Загружено ${count} из ${arr.length}`
            event.sender.send('downloaded', msg)
          }
        })
      }
    })
      .on('error', (err) => {
        console.log(116, err)
        let fss = require('fs')
        if (fss.existsSync(chapterPath)) {
          fs.remove(chapterPath)
        }
        downloadPages(event, arr, chapterPath, resolve)
        event.sender.send('download-error', 'Произошла ошибка во время загрузки страниц!')
      })
      .on('response', (response) => {
        if (status) {
          console.log(124, chapterPath, response.statusCode)
        }
      })
  })
}

export { parse }
