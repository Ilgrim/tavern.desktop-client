import { parse } from './parse-mangachan'

const ipc = require('electron').ipcMain
const fs = require('fs')
const jsonfile = require('jsonfile')

let profile = {invite: '', login: '', password: ''}
ipc.on('check-auth', (event) => {
  fs.stat('settings.json', (err, stats) => {
    if (err === null) {
      let obj = jsonfile.readFileSync('settings.json')
      if (obj.login !== '') {
        event.returnValue = { data: obj }
      } else {
        event.returnValue = { data: null }
      }
    } else if (err.code === 'ENOENT') {
      fs.writeFile('settings.json', JSON.stringify(profile))
      event.returnValue = { data: null }
    } else {
      console.log('Some other error: ', err.code)
    }
  })
})

ipc.on('request-chapters', (event, source) => {
  parse.chapters(source).then(
    success => {
      event.sender.send('response-chapters', success)
    },
    failure => {
      console.log(failure)
    })
})

ipc.on('request-pages', (event, chapter) => {
  parse.pages(event, chapter).then(
    success => {
      event.sender.send('response-pages', success)
    },
    failure => {
      console.log(failure)
    })
})
