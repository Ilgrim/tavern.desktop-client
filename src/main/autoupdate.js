import { autoUpdater } from 'electron-updater'

export { autoUpdater }

autoUpdater.on('checking-for-update', () => {
  console.log('Checking for update...')
})
autoUpdater.on('update-available', (info) => {
  console.log('Update available.')
})
autoUpdater.on('update-not-available', (info) => {
  console.log('Update not available.')
})
autoUpdater.on('error', (err) => {
  console.log('Error in auto-updater. ' + err)
})
autoUpdater.on('download-progress', (progressObj) => {
  let logMessage = 'Download speed: ' + progressObj.bytesPerSecond
  logMessage = logMessage + ' - Downloaded ' + progressObj.percent + '%'
  logMessage = logMessage + ' (' + progressObj.transferred + '/' + progressObj.total + ')'
  console.log(logMessage)
})
autoUpdater.on('update-downloaded', (info) => {
  console.log('Update downloaded')
})

autoUpdater.on('update-downloaded', () => {
  console.log('Update downloaded')
  autoUpdater.quitAndInstall()
})
